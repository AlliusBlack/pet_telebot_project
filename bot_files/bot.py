import telebot
import time
import random
from datetime import datetime
from config import API
from config import token

from keyboards import keyboard0, keyboard, keyboard1, keyboard2, keyboard3, keyboard4, keyboard5, keyboard6, keyboard7,\
    secret_keyboard, zod_today, zod_tomorrow, zod_week
from parser_currency import dollar, euro
from bot_files.parser_zodiac import predictions, predictions_today, predictions_tomorrow, predictions_week
from code_decode import code_func, decode_func
from openweather import get_weather

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def welcome_text(message):
    img = open("C:/Users/Perat/Desktop/w_photo.jpg", 'rb')
    bot.send_photo(message.chat.id, img, caption="Приветсвую тебя - {0.username}!\nЯ - <b>{1.first_name}</b>. Бот,"
                                                 " созданный специально для тех, кто любит тайны! 🔮"
                   .format(message.from_user, bot.get_me()), parse_mode='Html')
    time.sleep(3)
    bot.send_message(message.chat.id, f"Используй команду /help для вызова меню")


@bot.message_handler(commands=['help'])
def help_text(message):
    bot.send_message(message.chat.id, "Выбери интересующую тебя категорию ⬇", reply_markup=keyboard0)


@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.chat.type == 'private':

        # Главное меню
        if message.text == '🎲 Сыграть в кубаны':
            # Cube_game
            bot.send_message(message.chat.id, "Кидаю кубан...")
            time.sleep(2)
            user_score = str(random.randint(1, 6))
            bot_score = str(random.randint(1, 6))
            bot.send_message(message.from_user.id, 'У тебя выпало ' + user_score)
            time.sleep(2)
            bot.send_message(message.from_user.id, 'У меня выпало ' + bot_score)
            time.sleep(2)
            if user_score > bot_score:
                time.sleep(2)
                bot.send_message(message.from_user.id, 'Ты победил 🎉')
            elif user_score == bot_score:
                time.sleep(2)
                bot.send_message(message.from_user.id, 'У нас ничья 🤝')
            else:
                time.sleep(2)
                bot.send_message(message.from_user.id, 'Ты проиграл, увы 🌚')

        elif message.text == '🔮 Гороскоп':
            bot.send_message(message.chat.id, "Выбери интересующую категорию", reply_markup=keyboard)

        # Меню погоды и времени
        elif message.text == "🌞⌚ Погода и время":
            bot.send_message(message.from_user.id, 'Выбери категорию', reply_markup=keyboard6)
        elif message.text == "🌞 Погода":
            msg = bot.send_message(message.chat.id, 'Напиши название города, чтобы узнать погоду 🌤')
            bot.register_next_step_handler(msg, weather)
        elif message.text == "⌚ Время":
            now_time = datetime.now().strftime('%H:%M')
            bot.send_message(message.chat.id, "Московское время: " + str(now_time))

        # Курсы валют
        elif message.text == "💵 Курсы валют":
            bot.send_message(message.chat.id, "Выбери валюту 💰", reply_markup=keyboard7)
        elif message.text == "💵 Доллар":
            bot.send_message(message.chat.id, f"Курс доллара на {datetime.now().strftime('%H:%M')} мск.\n" +
                             str(dollar) + "\U000020BD")
        elif message.text == "💶 Евро":
            bot.send_message(message.chat.id, f"Курс евро на {datetime.now().strftime('%H:%M')} мск.\n" + str(euro)
                             + "\U000020BD")

        # Секретная кнопка
        elif message.text == "Секретная кнопка 🖲":
            bot.send_message(message.chat.id, "Ты попал в секретную комнату, где можно зашифровать или расшифровать"
                                              " записи...", reply_markup=secret_keyboard)
            time.sleep(2)
            bot.send_message(message.chat.id, "Выбери из списка, что тебе нужно сделать")
        elif message.text == "Кодировать 🔏":
            msg = bot.send_message(message.chat.id, "Введи свой текст ниже и отправь его мне, я его зашифрую")
            bot.register_next_step_handler(msg, code_res)
        elif message.text == "Декодировать 🔐":
            msg = bot.send_message(message.chat.id, "Введи или вставь свой текст ниже и отправь его мне, я его"
                                                    " расшифрую")
            bot.register_next_step_handler(msg, decode_res)

        # Общие прогнозы + прогнозы по знаку
        elif message.text == '🌐 Общие прогнозы':
            img1 = open("C:/Users/Perat/Desktop/interval_predictions.jpg", 'rb')
            bot.send_photo(message.chat.id, img1, caption="Выбери интервал прогноза 🗓", reply_markup=keyboard2)
        elif message.text == "👆 По знаку":
            bot.send_message(message.chat.id, "Выбери время для прогноза", reply_markup=keyboard3)
        elif message.text == '🔙 Назад':
            bot.send_message(message.chat.id, "Ты вернулся в главное меню!", reply_markup=keyboard0)
        else:
            bot.send_message(message.chat.id, "Не могу ответить на твой запрос")


def code_res(message):
    # Зашифровщик сообщений
    try:
        text = message.text
        b = code_func(text)
        bot.send_message(message.chat.id, "Зашифровываю записи...")
        time.sleep(3)
        bot.send_message(message.chat.id, b)
    except BaseException:
        bot.send_message(message.chat.id, "Произошла ошибка, неверный ввод данных!")


def decode_res(message):
    # Расшифровщик сообщений
    try:
        de_text = message.text
        b = decode_func(de_text)
        bot.send_message(message.chat.id, "Расшифровываю записи...")
        time.sleep(3)
        bot.send_message(message.chat.id, b)
    except BaseException:
        bot.send_message(message.chat.id, "Произошла ошибка, неверный ввод данных!")


def weather(message):
    # Weather_call
    try:
        user_input = message.text
        weather_result = get_weather(user_input, API)
        bot.send_message(message.chat.id, "Собираю прогноз...")
        time.sleep(3)
        bot.send_message(message.chat.id, weather_result)
    except Exception:
        bot.send_message(message.chat.id, 'Что-то пошло не так, проверьте правильность ввода')


@bot.callback_query_handler(func=lambda call: True)
def callback_buttons_predictions(call):
    if call.data == "Today":
        bot.send_message(call.message.chat.id, "Формирую прогноз на сегодня...")
        time.sleep(3)
        bot.send_message(call.message.chat.id, predictions[36])
    elif call.data == "Tomorrow":
        bot.send_message(call.message.chat.id, "Формирую прогноз на завтра...")
        time.sleep(3)
        bot.send_message(call.message.chat.id, predictions[37])
    elif call.data == "Week":
        bot.send_message(call.message.chat.id, "Формирую прогноз на неделю...")
        time.sleep(3)
        bot.send_message(call.message.chat.id, predictions[38])

    # Персональный прогноз на сегодня
    elif call.data == "Pers_today":
        bot.send_message(call.message.chat.id, 'Выбери знак зодиака', reply_markup=keyboard1)
    # Проходимся по списку знаков зодиака
    elif call.data in zod_today:
        a = zod_today.index(call.data)
        b = predictions_today[a]
        bot.send_message(call.message.chat.id, "Составляю прогноз...")
        time.sleep(3)
        bot.send_message(call.message.chat.id, b)
    # Персональный прогноз на завтра
    elif call.data == "Pers_tomorrow":
        bot.send_message(call.message.chat.id, 'Выбери знак зодиака', reply_markup=keyboard4)
    # Проходимся по списку знаков зодиака
    elif call.data in zod_tomorrow:
        a = zod_tomorrow.index(call.data)
        b = predictions_tomorrow[a]
        bot.send_message(call.message.chat.id, "Составляю прогноз...")
        time.sleep(3)
        bot.send_message(call.message.chat.id, b)
    # Персональный прогноз на неделю
    elif call.data == "Pers_week":
        bot.send_message(call.message.chat.id, 'Выбери знак зодиака', reply_markup=keyboard5)
    # Проходимся по списку знаков зодиака
    elif call.data in zod_week:
        a = zod_week.index(call.data)
        b = predictions_week[a]
        bot.send_message(call.message.chat.id, "Составляю прогноз...")
        time.sleep(3)
        bot.send_message(call.message.chat.id, b)
    else:
        bot.register_next_step_handler(call.message, help_text)


bot.polling(none_stop=True)

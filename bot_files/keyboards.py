# Клавиатуры

from telebot import types

# Главное меню
keyboard0 = types.ReplyKeyboardMarkup(resize_keyboard=True)
item1 = types.KeyboardButton("🔮 Гороскоп")
item2 = types.KeyboardButton("🌞⌚ Погода и время")
item3 = types.KeyboardButton("💵 Курсы валют")
item4 = types.KeyboardButton("🎲 Сыграть в кубаны")
item0 = types.KeyboardButton("Секретная кнопка 🖲")
keyboard0.row(item1, item2)
keyboard0.row(item3, item4)
keyboard0.row(item0)

# Выбор прогноза
keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
common = types.KeyboardButton("🌐 Общие прогнозы")
zodiac = types.KeyboardButton("👆 По знаку")
back = types.KeyboardButton("🔙 Назад")
keyboard.row(common, zodiac)
keyboard.row(back)

# Меню знаков зодиака на сегодня
keyboard1 = types.InlineKeyboardMarkup()
item5 = types.InlineKeyboardButton(text="♈ Овен", callback_data='Aries_today')
item6 = types.InlineKeyboardButton(text="♉ Телец", callback_data='Taurus_today')
item7 = types.InlineKeyboardButton(text="♊ Близнецы", callback_data='Gemini_today')
item8 = types.InlineKeyboardButton(text="♋ Рак", callback_data='Cancer_today')
item9 = types.InlineKeyboardButton(text="♌ Лев", callback_data='Leo_today')
item10 = types.InlineKeyboardButton(text="♍ Дева", callback_data='Virgo_today')
item11 = types.InlineKeyboardButton(text="♎ Весы", callback_data='Libra_today')
item12 = types.InlineKeyboardButton(text="♏ Скорпион", callback_data='Scorpio_today')
item13 = types.InlineKeyboardButton(text="♐ Стрелец", callback_data='Sagittarius_today')
item14 = types.InlineKeyboardButton(text="♑ Козерог", callback_data='Capricorn_today')
item15 = types.InlineKeyboardButton(text="♒ Водолей", callback_data='Aquarius_today')
item16 = types.InlineKeyboardButton(text="♓ Рыбы", callback_data='Pisces_today')
keyboard1.add(item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16)

zod_today = ['Aries_today', 'Taurus_today', 'Gemini_today', 'Cancer_today', 'Leo_today', 'Virgo_today', 'Libra_today',
       'Scorpio_today', 'Sagittarius_today', 'Capricorn_today', 'Aquarius_today', 'Pisces_today']

# Меню выбора даты для общих прогнозов
keyboard2 = types.InlineKeyboardMarkup()
today = types.InlineKeyboardButton(text="Сегодня", callback_data="Today")
tomorrow = types.InlineKeyboardButton(text="Завтра", callback_data="Tomorrow")
week = types.InlineKeyboardButton(text="Неделя", callback_data="Week")
keyboard2.add(today, tomorrow, week)


# Меню выбора даты персонального прогноза
keyboard3 = types.InlineKeyboardMarkup()
pers_today = types.InlineKeyboardButton(text="Сегодня", callback_data="Pers_today")
pers_tomorrow = types.InlineKeyboardButton(text="Завтра", callback_data="Pers_tomorrow")
pers_week = types.InlineKeyboardButton(text="Неделя", callback_data="Pers_week")
keyboard3.add(pers_today, pers_tomorrow, pers_week)


# Меню знаков зодиака на завтра
keyboard4 = types.InlineKeyboardMarkup()
item17 = types.InlineKeyboardButton(text="♈ Овен", callback_data='Aries_tomorrow')
item18 = types.InlineKeyboardButton(text="♉ Телец", callback_data='Taurus_tomorrow')
item19 = types.InlineKeyboardButton(text="♊ Близнецы", callback_data='Gemini_tomorrow')
item20 = types.InlineKeyboardButton(text="♋ Рак", callback_data='Cancer_tomorrow')
item21 = types.InlineKeyboardButton(text="♌ Лев", callback_data='Leo_tomorrow')
item22 = types.InlineKeyboardButton(text="♍ Дева", callback_data='Virgo_tomorrow')
item23 = types.InlineKeyboardButton(text="♎ Весы", callback_data='Libra_tomorrow')
item24 = types.InlineKeyboardButton(text="♏ Скорпион", callback_data='Scorpio_tomorrow')
item25 = types.InlineKeyboardButton(text="♐ Стрелец", callback_data='Sagittarius_tomorrow')
item26 = types.InlineKeyboardButton(text="♑ Козерог", callback_data='Capricorn_tomorrow')
item27 = types.InlineKeyboardButton(text="♒ Водолей", callback_data='Aquarius_tomorrow')
item28 = types.InlineKeyboardButton(text="♓ Рыбы", callback_data='Pisces_tomorrow')
keyboard4.add(item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28)

zod_tomorrow = ['Aries_tomorrow', 'Taurus_tomorrow', 'Gemini_tomorrow', 'Cancer_tomorrow', 'Leo_tomorrow',
                'Virgo_tomorrow', 'Libra_tomorrow', 'Scorpio_tomorrow', 'Sagittarius_tomorrow', 'Capricorn_tomorrow',
                'Aquarius_tomorrow', 'Pisces_tomorrow']


# Меню знаков зодиака на неделю
keyboard5 = types.InlineKeyboardMarkup()
item29 = types.InlineKeyboardButton(text="♈ Овен", callback_data='Aries_week')
item30 = types.InlineKeyboardButton(text="♉ Телец", callback_data='Taurus_week')
item31 = types.InlineKeyboardButton(text="♊ Близнецы", callback_data='Gemini_week')
item32 = types.InlineKeyboardButton(text="♋ Рак", callback_data='Cancer_week')
item33 = types.InlineKeyboardButton(text="♌ Лев", callback_data='Leo_week')
item34 = types.InlineKeyboardButton(text="♍ Дева", callback_data='Virgo_week')
item35 = types.InlineKeyboardButton(text="♎ Весы", callback_data='Libra_week')
item36 = types.InlineKeyboardButton(text="♏ Скорпион", callback_data='Scorpio_week')
item37 = types.InlineKeyboardButton(text="♐ Стрелец", callback_data='Sagittarius_week')
item38 = types.InlineKeyboardButton(text="♑ Козерог", callback_data='Capricorn_week')
item39 = types.InlineKeyboardButton(text="♒ Водолей", callback_data='Aquarius_week')
item40 = types.InlineKeyboardButton(text="♓ Рыбы", callback_data='Pisces_week')
keyboard5.add(item29, item30, item31, item32, item33, item34, item35, item36, item37, item38, item39, item40)

zod_week = ['Aries_week', 'Taurus_week', 'Gemini_week', 'Cancer_week', 'Leo_week', 'Virgo_week', 'Libra_week',
            'Scorpio_week', 'Sagittarius_week', 'Capricorn_week', 'Aquarius_week', 'Pisces_week']


keyboard6 = types.ReplyKeyboardMarkup(resize_keyboard=True)
weather_button = types.KeyboardButton("🌞 Погода")
now_time_button = types.KeyboardButton("⌚ Время")
back = types.KeyboardButton("🔙 Назад")
keyboard6.row(now_time_button, weather_button)
keyboard6.row(back)

keyboard7 = types.ReplyKeyboardMarkup(resize_keyboard=True)
dollar_button = types.KeyboardButton("💵 Доллар")
euro_button = types.KeyboardButton("💶 Евро")
back = types.KeyboardButton("🔙 Назад")
keyboard7.row(dollar_button, euro_button)
keyboard7.row(back)

# Меню секретной кнопки
secret_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
code_button = types.KeyboardButton("Кодировать 🔏")
decode_button = types.KeyboardButton("Декодировать 🔐")
back_button = types.KeyboardButton("🔙 Назад")
secret_keyboard.row(code_button, decode_button)
secret_keyboard.row(back_button)

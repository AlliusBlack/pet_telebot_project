import datetime
import requests

from config import API


def get_weather(user_input, API):

    code_smile = {
        "Clear": "Ясно \U0001F31D",
        "Clouds": "Облачно \U00002601",
        "Rain": "Дождь \U0001F327",
        "Drizzle": "Дождь \U0001F327",
        "Snow": "Снег \U0001F328",
        "Mist": "Туман \U0001F32B",
        "Tornado": "Торнадо \U0001F32A",
        "Thunderstorm": "Гроза \U0001F329",
    }
    try:
        url = f'https://api.openweathermap.org/data/2.5/weather?q={user_input}&appid={API}&units=metric'
        response = requests.get(url).json()

        city = response["name"]
        weather_description = response['weather'][0]['main']

        if weather_description in code_smile:
            wd = code_smile[weather_description]
        else:
            wd = 'На улице что-то странное...'

        cur_temp = round(response['main']['temp'])
        feels_temp = round(response['main']['feels_like'])

        if 0 <= feels_temp <= 10:
            weather_rec = 'Одевайся теплее, на улице холодно! \U0001F32C'
        elif 11 < feels_temp <= 20:
            weather_rec = 'На улице прохладно, возьми с собой куртку! \U0001F9E5'
        elif 20 < feels_temp < 25:
            weather_rec = 'На улице достаточно тепло! \U0001F455'
        elif feels_temp > 25:
            weather_rec = 'На улице жарко\U0001F321 возьми с собой воду \U0001F964 и головной убор! \U0001F9E2'
        else:
            weather_rec = 'Не замерзни! На улице очень холодно \U00002744'

        humidity = response['main']['humidity']
        wind = response['wind']['speed']

        a = (f'*** {datetime.datetime.now().strftime("%Y.%m.%d")} ***\n'
             f'Погода в городе - {city}\nТемпература: {cur_temp}С° {wd}\n'
             f'Ощущается, как: {feels_temp}С°\n'
             f'Влажность воздуха: {humidity}%\n'
             f'Скорость ветра: {wind} м/с\n\n'
             f'{weather_rec}\n'
             f'Хорошего тебя дня! \U0001F917')
        return a
    except Exception:
        b = 'Я не слышал о таком городе!\n' \
            'Проверь правильность названия города 🌃'
        return b

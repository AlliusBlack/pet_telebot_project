import time
import random

import requests
from bs4 import BeautifulSoup

predictions = []


def get_data(url):
    start_time = time.time()
    # headers = {
        # your-user-agent
    # }
    #
    # req = requests.get(url, headers=headers)

    # with open("main_page.html", "w", encoding="utf-8") as file:
    #     file.write(req.text)

    with open("main_page.html", encoding="utf-8") as file:
        src = file.read()

    soup = BeautifulSoup(src, "lxml")
    articles = soup.find_all("a", class_="p-imaged-item")
    common = soup.find_all("a", class_="filter__item")

    links_today_tomorrow_week = []
    comman_list = []

    for article in articles:
        link = "https://horo.mail.ru/" + article.get("href")
        links_today_tomorrow_week.append(link)
        links_today_tomorrow_week.append(link.replace('today', 'tomorrow'))
        links_today_tomorrow_week.append(link.replace('today', 'week'))

    # Zodiac_predictions
    for link in links_today_tomorrow_week:
        # time.sleep(random.randint(1, 3))
        # req = requests.get(link, headers)
        link_name = link.split("/")[-3]
        #
        # with open(f"data/{link_name}_{link.split('/')[-2]}.html", "w", encoding="utf-8") as file:
        #     file.write(req.text)
        #     print('Прогноз № ' + str(links_today_tomorrow_week.index(link) + 1) + ' готов!')

        with open(f"data/{link_name}_{link.split('/')[-2]}.html", encoding="utf-8") as file:
            src = file.read()

        soup = BeautifulSoup(src, "lxml")
        project_data = soup.find("div", class_="article__text").get_text(strip=True)
        predictions.append(project_data)

    # Common predictions
    for coma in common[1:4]:
        link = "https://horo.mail.ru/" + coma.get("href")
        comman_list.append(link)

    for link in comman_list:
        # time.sleep(random.randint(1, 3))
        # req = requests.get(link, headers)
        link_name = 'common' + '_' + str(link.split("/")[4]) + '_' + str(link.split("/")[-2])
        #
        # with open(f"data/{link_name}.html", "w", encoding="utf-8") as file:
        #     file.write(req.text)
        #     print('Общий прогноз № ' + str(comman_list.index(link) + 1) + ' готов!')

        with open(f"data/{link_name}.html", encoding="utf-8") as file:
            src = file.read()

        soup = BeautifulSoup(src, "lxml")
        project_data = soup.find("div", class_="article__text").get_text(strip=True)
        # print(project_data)
        predictions.append(project_data)

    end_time = round((time.time() - start_time), 2)
    print("\n" + "Время выполнения кода заняло " + str(end_time) + " c." + "\n")


get_data("https://horo.mail.ru/prediction/")

predictions_today = predictions[0:35:3]
predictions_tomorrow = predictions[1:35:3]
predictions_week = predictions[2:35:3]

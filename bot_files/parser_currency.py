# Курсы валют
import time

import requests
from bs4 import BeautifulSoup

URL = ["https://www.google.com/search?q=%D0%BA%D1%83%D1%80%D1%81+%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0+&rlz=1C1CHZN_ruRU925RU925&ei=CyMZYZu_HrKUxc8PmOy60A8&oq=%D0%BA%D1%83%D1%80%D1%81+%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0+&gs_lcp=Cgdnd3Mtd2l6EAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAELADEEMyBwgAELADEENKBAhBGABQxBZYxBZglhloAXACeACAAXmIAXmSAQMwLjGYAQCgAQHIAQrAAQE&sclient=gws-wiz&ved=0ahUKEwjb8Pj3nLPyAhUySvEDHRi2DvoQ4dUDCA4&uact=5",
       "https://www.google.com/search?q=%D0%BA%D1%83%D1%80%D1%81+%D0%B5%D0%B2%D1%80%D0%BE&rlz=1C1CHZN_ruRU925RU925&ei=BiMZYb3xPNaXxc8Pycai4AU&oq=%D0%BA%D1%83%D1%80%D1%81+%D0%B5%D0%B2%D1%80%D0%BE&gs_lcp=Cgdnd3Mtd2l6EAMyCAgAEIAEELEDMgsIABCABBCxAxCDATIICAAQgAQQsQMyBQgAEIAEMggIABCABBCxAzIICAAQgAQQsQMyCAgAEIAEELEDMggIABCABBCxAzIFCAAQsQMyBQgAEIAEOgcIABBHELADOgcIABCwAxBDOgcIABCxAxBDOgwIABCxAxBDEEYQggI6CQgAEIAEEAoQKjoOCAAQgAQQChAqEEYQggI6BwgAEIAEEAo6CAgAEBYQChAeOgsIABDJAxAWEAoQHjoJCAAQsQMQChAqOgcIABCxAxAKOgcIABDJAxAKOgUIABCSAzoECAAQCkoECEEYAFCKpllYrMdZYOrJWWgFcAJ4AYAB0gKIAZYUkgEIMS4xMi4xLjKYAQCgAQHIAQrAAQE&sclient=gws-wiz&ved=0ahUKEwi9jOb1nLPyAhXWS_EDHUmjCFwQ4dUDCA4&uact=5"]
HEADERS = {"User-Agent": "Your user-agent"}


d_e = []


for i in URL:
    r = requests.get(i, headers=HEADERS)
    soup = BeautifulSoup(r.content, 'html.parser')
    currency = soup.find('span', class_='DFlfde SwHCTb').get_text(strip=True)
    d_e.append(currency)

dollar = d_e[0]
euro = d_e[1]
